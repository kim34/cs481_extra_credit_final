﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Essentials;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;
using System.Collections.ObjectModel;


namespace CS481_HW_7
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Map : ContentPage
    {
        ObservableCollection<Pin> pinNames;

        public Map()
        {
            InitializeComponent();
            //initialize the map location. 
            var initialLocation = MapSpan.FromCenterAndRadius(new Position(33.1307785, -117.1601826), Distance.FromMiles(1));

            map.MoveToRegion(initialLocation);

            PopulatePicker();
        }



        //Zoom the map
        void ValueChanged(object sender, ValueChangedEventArgs e)
        {
            double zoomLevel = e.NewValue;
            double Degrees = 360 / (Math.Pow(2, zoomLevel));
            if (map.VisibleRegion != null)
            {
                map.MoveToRegion(new MapSpan(map.VisibleRegion.Center, Degrees, Degrees));
            }
        }

        // Street.Satellite and Hybrid button for the map 
        void OnButtonClicked(object sender, EventArgs e)
        {
            Button locationType = sender as Button;
            switch (locationType.Text)
            {
                case "Street":
                    map.MapType = MapType.Street;
                    break;

                case "Satellite":
                    map.MapType = MapType.Satellite;
                    break;

                case "Hybrid":
                    map.MapType = MapType.Hybrid;
                    break;
            }
        }

        // Make four pins
        private void PopulatePicker()
        {
            //Instantiate five pins for the map
            pinNames = new ObservableCollection<Pin>
            {
                new Pin
                {
                    Type =PinType.SavedPin, Label="Coachella", Address="Friday, October 9, 2020  Coachella Valley Music and Arts Festival will begin on",
                    Position = new Position(33.688035, -116.175375)
                },
                new Pin
                {
                    Type =PinType.SavedPin, Label="Vaudeville", Address="Saturday, June 27, 2020 The US Grant Hotel Downtown San Diego",
                    Position = new Position(32.754971,-117.143021)
                },
                new Pin
                {
                    Type=PinType.SavedPin, Label="Lollapalooza", Address="30 July-2 August 2020; Grant Park, Chicago",
                    Position = new Position(41.873530,-87.620961)
                },
                new Pin
                {
                    Type = PinType.SavedPin, Label="URGE Pub", Address="255 Redel Rd",
                    Position = new Position(33.1358939, -117.15856439999999)
                },
                new Pin
                {
                    Type = PinType.SavedPin, Label="Movie Theater", Address="1180 W San Marcos Blvd",
                    Position = new Position(33.13507480000001, -117.1928289)
                }
            };

            mypicker.Items.Add("---");
            foreach (Pin item in pinNames)
            {
                //Load each pin into the map and picker
                mypicker.Items.Add(item.Label);
                map.Pins.Add(item);
            }


        }

        private void PinClicked(object sender, System.EventArgs e)
        {
            var inputSlot = (Picker)sender;
            string pname = inputSlot.SelectedItem.ToString();
            //When user selects a pin name, find the pin
            foreach (Pin item in pinNames)
            {
                if (item.Label == pname)
                {
                    //Move the map to center on the pin
                    map.MoveToRegion(MapSpan.FromCenterAndRadius(item.Position, Distance.FromMiles(1)).WithZoom(19));
                }
            }

        }






    }
}
