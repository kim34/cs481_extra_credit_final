﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW_7
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Tabpage : TabbedPage
    {
        public Tabpage()
        {
            InitializeComponent();
        }
    }
}