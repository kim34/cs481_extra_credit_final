﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW_7
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Data : ContentPage
    {
        public Data()
        {
            InitializeComponent();

        }
            private void datetime_Clicked(object sender, EventArgs e)
            {
                var date = dt.Date; //set a new date from user
                var time = tm.Time; // set a new time from user
                Details.Text = string.Format("Date: {0} \nTime : {1}", date, time); // display selected date and time from user
            }
        }
    }
