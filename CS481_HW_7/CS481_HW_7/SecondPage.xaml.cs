﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW_7
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SecondPage : ContentPage
    {
        public SecondPage()
        {
            InitializeComponent();
        }

        public async void NextPage(object sender, EventArgs e)
        {

            int ID = Convert.ToInt32(Entry_ID.Text); // change the ID.text to intiger. 


            if (Entry_ID == null || ID < 0 || Entry_Name == null)  // if Id is empty or less tan 0, give alert. 
            {
                await DisplayAlert("Please Insert Student ID", " ID is empty", "OK"); // display warning if there is no student ID. 
                return;
            }

            if (ID <= 0)    // if the inserted ID is less than 0 give alert
            {
                await DisplayAlert("ID is not valid", "Please Intert valid ID", "OK");
                return;
            }
            await Navigation.PushAsync(new Information(Entry_Name.Text, ID)); // Enter the student ID then go to next page (third page).

        }
    }
}