﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW_7
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Information : ContentPage
    {
        public Information(string name, int ID)
        {
            InitializeComponent();

            label_text.Text = string.Format("Hello! {0} / {1}! Welcome to Mobile Programing class", name, ID); // Display user's name and student ID.

        }

        public async void GoTab(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Tabpage()); // if clicked the GO First button, then go to first page
        }
    }
}